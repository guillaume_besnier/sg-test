public class Dancer {
    public static class Algo {
        static int getPos(int n) {
            float step = n % 6;
            float step1 = 0;
            if(step == step1) return 0;
            float step2 = 1 % 6;
            if(step == step2) return 1;
            float step3 = 2 % 6;
            if(step == step3) return -1;
            float step4 = 3 % 6;
            if(step == step4) return -4;
            float step5 = 4 % 6;
            if(step == step5) return -5;
            float step6 = 5 % 6;
            if(step == step6) return -3;
            throw new RuntimeException("impossible");
        }
    }

    public static void main(String... args) {
        System.out.println(Algo.getPos(0));
        System.out.println(Algo.getPos(1));
        System.out.println(Algo.getPos(2));
        System.out.println(Algo.getPos(3));
        System.out.println(Algo.getPos(4));
        System.out.println(Algo.getPos(5));
        System.out.println(Algo.getPos(6));
        System.out.println(Algo.getPos(7));
        System.out.println(Algo.getPos(100000));
        System.out.println(Algo.getPos(2147483647));
    }
}

