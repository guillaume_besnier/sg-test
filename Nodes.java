public class Nodes {
    public static class Node {
        Node left, right;
        int value;

        public Node(int value, Node l, Node r){
            this.value = value;
            this.left = l;
            this.right = r;
        }

        public Node find(int v){
            Node node = this;

            while(node != null && node.value != v){
                node = node.value > v ? node.left : node.right;
            }

            return node;
        }
    }

    public static void main(String... args) {
        Node n6 = new Node(6, null, null);
        Node n2 = new Node(2, null, null);
        Node n16 = new Node(16, null, null);
        Node n5 = new Node(5, n2, n6);
        Node n17 = new Node(17, n16, null);
        Node n8 = new Node(8, null, null);
        Node n7 = new Node(7, n5, n8);
        Node n13 = new Node(13, null, n17);
        Node n = new Node(9, n7, n13);

        Node r = n.find(9);
        System.out.println("r:"+r.value);
    }
}
